var app = require('express').createServer(),
	express = require('express'),
	Ecomcom = require('ecomcom');
var ecom = new Ecomcom(),
	acct = ecom.account({
	'amazon': {
		'awsId': 'AWS ID',
		'awsSecret': 'SECRET',
		'assocId': 'ASSOCIATE ID'
	},
	'google': 'GOOGLE ASSOCIATE ID'
});

app.configure('development', function(){
    app.use("/static", express.static(__dirname + '/static'));
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.use(express.bodyParser());
    app.use(express.logger());
});

app.set('view engine', 'jade');
app.set('view options', {
  layout: false
});

app.configure('production', function(){
  var oneYear = 31557600000;
  app.use(express.static(__dirname + '/static', { maxAge: oneYear }));
  app.use(express.errorHandler());
});

app.get('/', function(req, res){
  res.render('lookup');
});

app.get('/sample', function(req, res){
  res.render('sample');
});

app.get('/lookup/:idtype/:id', function(req, res){
	//console.log('Lookup on: ' + req.param('id') + ' ' + req.param('idType'));
	console.log('Lookup on: ' + req.params.id)
	acct.lookup({
		'id': req.params.id,
		'idType': req.params.idtype
	}, function(err, data) {
		if(err){
			console.log('ERROR: ' + err.message);
			return res.send({'failed': err.message});
		} else {
			return res.send(data);
		}
	});
});

/*
	attrs is an array of key:value pairs separated by the pipe
	ex: /lookup/UPC/797240564309/brand:merrell|size:12|color:ink
*/
app.get('/lookup/:idtype/:id/:attrs', function(req, res) {
	var attributes = {};
	req.params.attrs.split('|').forEach(function(el, ind){var arg = el.split(':'); attributes[arg[0]] = arg[1];});
	console.log('Lookup on: ' + req.params.id)
	console.log(attributes);
	acct.lookup({
		'id': req.params.id,
		'idType': req.params.idtype,
		'attributes' : attributes
	}, function(err, data) {
		if(err){
			console.log('ERROR: ' + err.message);
			return res.send({'failed': err.message});
		} else {
			return res.send(data);
		}
	});
});

app.listen(3001);
