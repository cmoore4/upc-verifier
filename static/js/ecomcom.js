$(document).ready(function() {

	/*$('#id').blur(function(ev) {
		var value = $(this).val();
		if(value.length === 12){
			ecomcom.lookup(value, 'UPC');
			$('#infoBox').text('Detected UPC, getting product info...');
		}
	});*/
	$('#id').keypress(function(ev) {
		var value = $(this).val();
		if(ev.which == 13){
			ev.preventDefault();
			ecomcom.lookup(value, 'UPC');
			$('#infoBox').text('Retrieving results');
		}
	});

	$('#submitUPC').click(function(ev) {
		var value = $('#id').val();
		ev.preventDefault();
		ecomcom.lookup(value, 'UPC');
		$('#infoBox').text('Retrieving results');
	});

	
	$('#json').modal({show: false});
	$('#viewJson').click(function(ev) {
		ev.preventDefault();
		$('#json').toggle();
	});
	$('.jsonClose').click(function(ev) {
		ev.preventDefault();
		$('#json').toggle();
	});

	parseURL();
	window.onpopstate = function(event){parseURL();}


});

var ecomcom = {
	lookup: function(id, idType){
		$.ajax({
			'contentType': 'application/x-www-form-urlencoded',
			'success': function(data) {
				ecomcom.processResponse(data);
			},
			'url': '/lookup/' + idType + '/' + id
		});
		if(typeof (history) === "object"){
			history.pushState({'upc' : id, 'upcType' : idType}, 'eComCom Result', '#/'+ idType + '/' + id);
			window.document.title = 'eComCom: ' + id;
		}
	},

	processResponse: function(item){
		$('#jsonBody').html(syntaxHighlight(item));

		$('#barcode').barcode($('#id').val(), 'ean13', {barHeight: 80, barWidth: 2});

		$('#infoBox').text('');
		if(item.amzError){
			if(item.amzError.indexOf('is not a valid value for ItemId')){
				$('#infoBox').text('Amazon could not find that item number.');
			}
			$('#infoBox').text('Error looking up product: ' + item.amzError);
		}

		var prod = {
			'title': '',
			'metadata': '',
			'upc': ''
		};
		if(!((item.brand && !item.model) || (!item.model && item.brand))){
			prod.title = ((item.brand) ? item.brand + ' ' : '') + item.model.replace(item.brand, '');
		} else {
			var sortNames = item.names;
			prod.title = sortNames.sort(function (a, b) { return b.length - a.length; })[0];
		}
		prod.title = prod.title.toTitleCase();
		if(item.dept){prod.metadata += item.dept.toTitleCase();}
		if(prod.metadata.length > 0){prod.metadata += ' ';}
		if(item.size){prod.metadata += 'Size ' + item.size;}
		if(prod.metadata.length > 0){prod.metadata +=', ';}
		if(item.color){prod.metadata += item.color.toTitleCase();}
		if(item.upc){prod.upc = '#' + item.upc.toString();}
		if(item.category){prod.category = item.category.toTitleCase();}

		$('#productTitle').text(prod.title);
		$('#productType').text(prod.category);
		$('#productMetaInfo').html(prod.metadata + '&nbsp');
		$('#productMetaId').text(prod.upc);

		$('#ecomMeta h3').removeClass('hidden');
		$('#viewJson').removeClass('hidden');

		$('#names').html('');
		for (var i = item.names.length - 1; i >= 0; i--) {
			$('#names').append('<li>' + item.names[i] + '<li>');
		}
		$('#prices').html('');
		for (i = item.prices.length - 1; i >= 0; i--) {
			$('#prices').append('<li>' + item.prices[i].toFixed(2) + '<li>');
		}
		$('#sellers').html('');
		for (i = item.stores.length - 1; i >= 0; i--) {
			$('#sellers').append('<li>' + item.stores[i] + '<li>');
		}

	}
};

String.prototype.toTitleCase = function () {
	var A = this.split(' '), B = [];
	for (var i = 0; A[i] !== undefined; i++) {
		B[B.length] = A[i].substr(0, 1).toUpperCase() + A[i].substr(1);
	}
	return B.join(' ');
};

function syntaxHighlight(obj) {
	var json = JSON.stringify(obj, undefined, 5);
    json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return '<pre>' + json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    }) + '</pre>';
}

function parseURL(){
	var urlArgs = window.location.toString().split('#');
	if(urlArgs[1]){
		var params = urlArgs[1].split('/');
		// params[0] is an smpty string due to the lead character being a '/'
		if(params.length === 3 || params.length === 4){
			ecomcom.lookup(params[2], params[1]);
			$('#id').val(params[2]);
		}
	}

	return false;
}